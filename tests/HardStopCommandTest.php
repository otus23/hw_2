<?php

use PHPUnit\Framework\TestCase;
use Romqa\Commands\HardStopCommand;
use Romqa\Commands\RunThreadCommand;
use Romqa\Contracts\Command;
use Romqa\Exception\HardStopThreadException;
use Romqa\Threads\SimpleThread;

class HardStopCommandTest extends TestCase
{
    public function testHardStopping(): void
    {
        $commands = [
            $this->createMock(Command::class),
            $this->createMock(Command::class),
            $this->createMock(Command::class),
            $this->createMock(Command::class),
        ];

        $thread = new SimpleThread($commands);
        $command_start = new RunThreadCommand($thread);
        $command_start->execute();

        $this->expectException(HardStopThreadException::class);
        $command = new HardStopCommand($thread);
        $command->execute();

        $this->assertEquals($thread->getFiber()->isTerminated(), true);
    }
}