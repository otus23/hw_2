<?php

use PHPUnit\Framework\TestCase;
use Romqa\Commands\RunThreadCommand;
use Romqa\Contracts\Command;
use Romqa\Exception\CommandException;
use Romqa\Threads\SimpleThread;

class RunThreadCommandTest extends TestCase
{
    public function testRunThread(): void
    {
        $commands = [
            $this->createMock(Command::class),
            $this->createMock(Command::class),
            $this->createMock(Command::class),
            $this->createMock(Command::class),
        ];

        $thread = new SimpleThread($commands);

        $command = new RunThreadCommand($thread);
        $command->execute();

        $this->assertEquals($thread->getFiber()->isStarted(), true);
    }

    public function testRunWithCommandExceptionThread(): void
    {
        $commands = [
            $this->createMock(Command::class)
                ->method('execute')
                ->willThrowException(new CommandException('test')),
            $this->createMock(Command::class),
            $this->createMock(Command::class),
            $this->createMock(Command::class),
        ];

        $thread = new SimpleThread($commands);

        $command = new RunThreadCommand($thread);
        $command->execute();

        $this->assertEquals($thread->getFiber()->isStarted(), true);
    }
}