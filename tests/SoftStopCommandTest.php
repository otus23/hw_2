<?php

use PHPUnit\Framework\TestCase;
use Romqa\Commands\RunThreadCommand;
use Romqa\Commands\SoftStopCommand;
use Romqa\Contracts\Command;
use Romqa\Threads\SimpleThread;

class SoftStopCommandTest extends TestCase
{
    public function testSoftStopping(): void
    {
        $commands = [
            $this->createMock(Command::class),
            $this->createMock(Command::class),
            $this->createMock(Command::class),
            $this->createMock(Command::class),
        ];

        $thread = new SimpleThread($commands);

        $command = new RunThreadCommand($thread);
        $command->execute();

        $command = new SoftStopCommand($thread);
        $command->execute();

        $this->assertEquals($thread->getFiber()->isRunning(), false);
    }
}