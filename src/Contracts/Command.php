<?php

namespace Romqa\Contracts;

interface Command
{
    public function execute(): void;
}