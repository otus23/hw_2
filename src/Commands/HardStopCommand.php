<?php

namespace Romqa\Commands;

use Fiber;
use Romqa\Contracts\Command;
use Romqa\Exception\HardStopThreadException;
use Romqa\Threads\SimpleThread;

class HardStopCommand implements Command
{
    public function __construct(protected SimpleThread $thread)
    {
    }

    /**
     * @throws HardStopThreadException|\Throwable
     */
    public function execute(): void
    {
        $this->thread->getFiber()->throw(new HardStopThreadException());
    }
}