<?php

namespace Romqa\Commands;

use Romqa\Contracts\Command;
use Romqa\Threads\SimpleThread;

class RunThreadCommand implements Command
{
    public function __construct(protected SimpleThread $thread)
    {
    }

    /**
     * @throws \Throwable
     */
    public function execute(): void
    {
        $this->thread->getFiber()->start($this->thread);
    }
}