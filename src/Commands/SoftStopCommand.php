<?php

namespace Romqa\Commands;

use Romqa\Contracts\Command;
use Romqa\Threads\SimpleThread;

class SoftStopCommand implements Command
{
    public function __construct(protected SimpleThread $thread)
    {
    }

    public function execute(): void
    {
        $this->thread->getFiber()->resume(false);
    }
}