<?php

namespace Romqa\Exception;

use Exception;
use Throwable;

class CommandException extends Exception
{
    protected string $type;

    public function __construct(string $type, string $message = "", int $code = 0, ?Throwable $previous = null)
    {
        $this->type = $type;

        parent::__construct($message, $code, $previous);
    }

    public function getType(): string
    {
        return $this->type;
    }
}