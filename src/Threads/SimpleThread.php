<?php

namespace Romqa\Threads;

use Fiber;
use Romqa\Contracts\Command;
use Romqa\Exception\CommandException;

class SimpleThread
{
    /* @var Command[] */
    protected array $commands;
    protected Fiber $fiber;

    public function __construct(array $commands)
    {
        $this->commands = $commands;
        $this->fiber = new Fiber(function (SimpleThread $thread) {
            $thread->run();
        });
    }

    public function run(): void
    {
        while ($command = array_pop($this->commands)) {
            try {
                $command->execute();
            } catch (CommandException $e) {

            }
            if (!Fiber::suspend()) {
                break;
            }
        }
    }

    public function getFiber()
    {
        return $this->fiber;
    }
}